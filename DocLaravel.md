## Commandes

- Lancer server Laravel : ```php artisan serv```

- Installer Spatie : ```composer require spatie/laravel-permission```
- Installer Larecipe : ```composer require binarytorch/larecipe``` and ``` php artisan larecipe:install```
- Créer un Middleware : ```php artisan make:middleware NomMiddle```   
- Installation de Larecipe pour avoir acces au laravel/docs : ```composer require binarytorch/larecipe ``` and lauch ``` php artisan larecipe:install```   
- Création table dans base de donnée : ```php artisan make:migration create_users_tables```
- Destruction de la base donnée et réimplémentation des tables et seeders : ```php artisan migrate:refresh --seed```   

## Les dossiers
- APP :     
Ce dossier contient les applications c'est à dire les fonctions, classes, etc...(tout le code PHP en gros).
- BOOTSTRAP :  
Contient les fichiers pour lancer le framework et un dossier en cache.
- CONFIG :    
Permet de configurer le framework tel que la base de donnée, les views, etc...
- DATABASE :  
Ce dossier permet de gérer la base de donnée intégralement grace au sous-dossier ```migration``` qui décrit le contenu de la base de donnée, et du ```seeds``` qui permet de run les tables dans le dossier ```migration```.     
- ROUTES :  
Ce dossier permet de faire les liaisons entre les pages web du dossier ```views``` qui doivent toutes contenir un ```.blade```(Ex: home.blade.php ).  
## Les fichiers  
- .env :   
Ce fichier contient les mots de passe de l'application donc ne doit jamais être partagé. Pour connaitre les informations il y a un fichier ```.env.example``` qui contient les valeurs de base du ```.env```.
- artisan :      
Ce fichier permet l'utilisation des commandes artisan.  
- server.php :    
Permet l'utilisation de la commande ```php artisan serv``` qui lance Laravel en local.

## Exercices

- Création de plusieurs page (ex:contact.blade.php)
  - Liaisons des plusieurs pages en faisant des routes.  

```Route::get('/', function () {   
    return view('welcome');    
});
```
 

  - Création de 100 utilisateurs random dans une base de donnée pour qu'ils puissent se connecter.
  
```
    class UsersTableSeeder extends Seeder    
{    
     @return void        
     public function run(){    
    factory(App\User::class, 100)->create();    
    }     
}
```    

  - Création de different role pour chaque utilisateurs.   

```
$role = Role::create(['name' => 'role1']);
```
  - Selon les roles attribués, les utilisateurs ont des privilèges sur les pages.    
```
$role->givePermissionTo($permission);  
$permission->assignRole($role);
```

- Utiliser les Middleware   
Se situe ici : ```app/Http/Kernel.php```   

Création des roles dans un seeder : (RolesTableSeeder)   
```
class RolesTableSeeder extends Seeder   
    {   
/**   
 * Run the database seeds.   
 *     
 * @return void   
 */     
 public function run()    
    {    
        Role::create(['name' => 'role1']);    
        Role::create(['name' => 'role2']);    
        Role::create(['name' => 'role3']);   
    }   
}
```    

Attribution des roles : (usersTAbleSeeder)   
```
public function run() 
{    
/**   
* factory(App\User::class, 100)->create();   
*/   
factory(App\User::class, 10)->create()->each(function ($user) {    
$user->assignRole('user1');
```    

Redirection en fonction des roles : (CheckRole)   
```
public function handle($request, Closure $next, $role)   
{    
    if (! $request->user()->hasRole('role1')) {   
// Redirect...
```   


